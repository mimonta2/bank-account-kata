$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("com/bank/service/deposit.feature");
formatter.feature({
  "line": 1,
  "name": "Save money",
  "description": "",
  "id": "save-money",
  "keyword": "Feature"
});
formatter.before({
  "duration": 54945863,
  "status": "passed"
});
formatter.before({
  "duration": 3792865,
  "status": "passed"
});
formatter.before({
  "duration": 3521732,
  "status": "passed"
});
formatter.scenario({
  "line": 3,
  "name": "Depositing money on a new account",
  "description": "",
  "id": "save-money;depositing-money-on-a-new-account",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "a User has an account to credit with the initial transactions",
  "rows": [
    {
      "cells": [
        "date",
        "amount",
        "balance"
      ],
      "line": 5
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "\u002750.0\u0027 are deposited into the account",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "the balance should be \u002750.0\u0027",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "the Transactions count should be equal to \u00271\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "the Deposit RC should be 200",
  "keyword": "And "
});
formatter.match({
  "location": "DepositStepDefinitions.a_User_has_an_account_with_initial_transactions(String,String\u003e\u003e)"
});
formatter.result({
  "duration": 112519234,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "50.0",
      "offset": 1
    }
  ],
  "location": "DepositStepDefinitions.are_deposited_into_the_account(float)"
});
formatter.result({
  "duration": 111523655,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "50.0",
      "offset": 23
    }
  ],
  "location": "DepositStepDefinitions.the_balance_should_be(float)"
});
formatter.result({
  "duration": 1273555,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 43
    }
  ],
  "location": "DepositStepDefinitions.the_Transactions_count_should_be_equal_to(long)"
});
formatter.result({
  "duration": 568780,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 25
    }
  ],
  "location": "DepositStepDefinitions.the_Deposit_RC_should_be(int)"
});
formatter.result({
  "duration": 289521,
  "status": "passed"
});
formatter.before({
  "duration": 3053023,
  "status": "passed"
});
formatter.before({
  "duration": 3546964,
  "status": "passed"
});
formatter.before({
  "duration": 3108618,
  "status": "passed"
});
formatter.scenario({
  "line": 11,
  "name": "Depositing money on an existing account",
  "description": "",
  "id": "save-money;depositing-money-on-an-existing-account",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 12,
  "name": "a User has an account to credit with the initial transactions",
  "rows": [
    {
      "cells": [
        "date",
        "amount",
        "balance"
      ],
      "line": 13
    },
    {
      "cells": [
        "26/06/2016 10:10:20",
        "60.0",
        "60.0"
      ],
      "line": 14
    },
    {
      "cells": [
        "26/06/2016 12:30:20",
        "30.0",
        "90.0"
      ],
      "line": 15
    },
    {
      "cells": [
        "26/06/2016 19:49:11",
        "-70.0",
        "20.0"
      ],
      "line": 16
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 17,
  "name": "\u002750.0\u0027 are deposited into the account",
  "keyword": "When "
});
formatter.step({
  "line": 18,
  "name": "the balance should be \u002770.0\u0027",
  "keyword": "Then "
});
formatter.step({
  "line": 19,
  "name": "the Transactions count should be equal to \u00274\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "the Deposit RC should be 200",
  "keyword": "And "
});
formatter.match({
  "location": "DepositStepDefinitions.a_User_has_an_account_with_initial_transactions(String,String\u003e\u003e)"
});
formatter.result({
  "duration": 9935689,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "50.0",
      "offset": 1
    }
  ],
  "location": "DepositStepDefinitions.are_deposited_into_the_account(float)"
});
formatter.result({
  "duration": 7175182,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "70.0",
      "offset": 23
    }
  ],
  "location": "DepositStepDefinitions.the_balance_should_be(float)"
});
formatter.result({
  "duration": 640199,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "4",
      "offset": 43
    }
  ],
  "location": "DepositStepDefinitions.the_Transactions_count_should_be_equal_to(long)"
});
formatter.result({
  "duration": 387027,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 25
    }
  ],
  "location": "DepositStepDefinitions.the_Deposit_RC_should_be(int)"
});
formatter.result({
  "duration": 76550,
  "status": "passed"
});
formatter.uri("com/bank/service/statement.feature");
formatter.feature({
  "line": 1,
  "name": "Print Transactions History",
  "description": "",
  "id": "print-transactions-history",
  "keyword": "Feature"
});
formatter.before({
  "duration": 1276548,
  "status": "passed"
});
formatter.before({
  "duration": 2151956,
  "status": "passed"
});
formatter.before({
  "duration": 2806267,
  "status": "passed"
});
formatter.scenario({
  "line": 3,
  "name": "Printing an account statement",
  "description": "",
  "id": "print-transactions-history;printing-an-account-statement",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "a User has an account to print with the initial transactions",
  "rows": [
    {
      "cells": [
        "date",
        "amount",
        "balance"
      ],
      "line": 5
    },
    {
      "cells": [
        "26/04/2016 10:10:20",
        "60",
        "60"
      ],
      "line": 6
    },
    {
      "cells": [
        "27/04/2016 12:30:20",
        "300",
        "360"
      ],
      "line": 7
    },
    {
      "cells": [
        "28/04/2016 19:49:11",
        "-60",
        "300"
      ],
      "line": 8
    },
    {
      "cells": [
        "03/05/2016 10:49:11",
        "-40",
        "260"
      ],
      "line": 9
    },
    {
      "cells": [
        "09/05/2016 12:39:11",
        "220",
        "480"
      ],
      "line": 10
    },
    {
      "cells": [
        "15/05/2016 19:11:11",
        "-70",
        "410"
      ],
      "line": 11
    },
    {
      "cells": [
        "22/05/2016 13:22:11",
        "-90",
        "320"
      ],
      "line": 12
    },
    {
      "cells": [
        "15/06/2016 19:11:11",
        "70",
        "390"
      ],
      "line": 13
    },
    {
      "cells": [
        "26/06/2016 13:22:11",
        "90",
        "480"
      ],
      "line": 14
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 15,
  "name": "the user prints the statement of the month \u002705/2016\u0027",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "the statement should be",
  "rows": [
    {
      "cells": [
        "Operation",
        "Date",
        "Amount",
        "Balance"
      ],
      "line": 17
    },
    {
      "cells": [
        "D",
        "03/05/2016 10:49:11",
        "40",
        "260"
      ],
      "line": 18
    },
    {
      "cells": [
        "C",
        "09/05/2016 12:39:11",
        "220",
        "480"
      ],
      "line": 19
    },
    {
      "cells": [
        "D",
        "15/05/2016 19:11:11",
        "70",
        "410"
      ],
      "line": 20
    },
    {
      "cells": [
        "D",
        "22/05/2016 13:22:11",
        "90",
        "320"
      ],
      "line": 21
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "StatementStepDefinitions.a_User_has_an_account_with_initial_transactions(String,String\u003e\u003e)"
});
formatter.result({
  "duration": 19671664,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "05",
      "offset": 44
    },
    {
      "val": "2016",
      "offset": 47
    }
  ],
  "location": "StatementStepDefinitions.the_user_prints_the_statement_of_the_month(int,int)"
});
formatter.result({
  "duration": 41567558,
  "status": "passed"
});
formatter.match({
  "location": "StatementStepDefinitions.the_statement_should_be(String,String\u003e\u003e)"
});
formatter.result({
  "duration": 5232776,
  "status": "passed"
});
formatter.before({
  "duration": 2431642,
  "status": "passed"
});
formatter.before({
  "duration": 1882961,
  "status": "passed"
});
formatter.before({
  "duration": 1753810,
  "status": "passed"
});
formatter.scenario({
  "line": 24,
  "name": "Printing an account statement",
  "description": "",
  "id": "print-transactions-history;printing-an-account-statement",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 25,
  "name": "a User has an account to print with the initial transactions",
  "rows": [
    {
      "cells": [
        "date",
        "amount",
        "balance"
      ],
      "line": 26
    },
    {
      "cells": [
        "26/04/2016 10:10:20",
        "60",
        "60"
      ],
      "line": 27
    },
    {
      "cells": [
        "27/04/2016 12:30:20",
        "300",
        "360"
      ],
      "line": 28
    },
    {
      "cells": [
        "28/04/2016 19:49:11",
        "-60",
        "300"
      ],
      "line": 29
    },
    {
      "cells": [
        "03/05/2016 10:49:11",
        "-40",
        "260"
      ],
      "line": 30
    },
    {
      "cells": [
        "09/05/2016 12:39:11",
        "220",
        "480"
      ],
      "line": 31
    },
    {
      "cells": [
        "15/05/2016 19:11:11",
        "-70",
        "410"
      ],
      "line": 32
    },
    {
      "cells": [
        "22/05/2016 13:22:11",
        "-90",
        "320"
      ],
      "line": 33
    },
    {
      "cells": [
        "15/06/2016 19:11:11",
        "70",
        "390"
      ],
      "line": 34
    },
    {
      "cells": [
        "26/06/2016 13:22:11",
        "90",
        "480"
      ],
      "line": 35
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 36,
  "name": "the user prints the Operations History between \u002703/05/2016\u0027 and \u002715/05/2016\u0027",
  "keyword": "When "
});
formatter.step({
  "line": 37,
  "name": "the statement should be",
  "rows": [
    {
      "cells": [
        "Operation",
        "Date",
        "Amount",
        "Balance"
      ],
      "line": 38
    },
    {
      "cells": [
        "D",
        "03/05/2016 10:49:11",
        "40",
        "260"
      ],
      "line": 39
    },
    {
      "cells": [
        "C",
        "09/05/2016 12:39:11",
        "220",
        "480"
      ],
      "line": 40
    },
    {
      "cells": [
        "D",
        "15/05/2016 19:11:11",
        "70",
        "410"
      ],
      "line": 41
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "StatementStepDefinitions.a_User_has_an_account_with_initial_transactions(String,String\u003e\u003e)"
});
formatter.result({
  "duration": 11959349,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "03/05/2016",
      "offset": 48
    },
    {
      "val": "15/05/2016",
      "offset": 65
    }
  ],
  "location": "StatementStepDefinitions.the_user_prints_the_Operations_History_between_and(Date,Date)"
});
formatter.result({
  "duration": 6809109,
  "status": "passed"
});
formatter.match({
  "location": "StatementStepDefinitions.the_statement_should_be(String,String\u003e\u003e)"
});
formatter.result({
  "duration": 3512751,
  "status": "passed"
});
formatter.uri("com/bank/service/withdraw.feature");
formatter.feature({
  "line": 1,
  "name": "Withdraw money",
  "description": "",
  "id": "withdraw-money",
  "keyword": "Feature"
});
formatter.before({
  "duration": 3375474,
  "status": "passed"
});
formatter.before({
  "duration": 3677826,
  "status": "passed"
});
formatter.before({
  "duration": 3987875,
  "status": "passed"
});
formatter.scenario({
  "line": 3,
  "name": "Withdrawing money with sufficient funds",
  "description": "",
  "id": "withdraw-money;withdrawing-money-with-sufficient-funds",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "a User has an account to debit with the initial transactions",
  "rows": [
    {
      "cells": [
        "date",
        "amount",
        "balance"
      ],
      "line": 5
    },
    {
      "cells": [
        "26/06/2016 10:10:20",
        "60.0",
        "60.0"
      ],
      "line": 6
    },
    {
      "cells": [
        "26/06/2016 12:30:20",
        "30.0",
        "90.0"
      ],
      "line": 7
    },
    {
      "cells": [
        "26/06/2016 19:49:11",
        "-70.0",
        "20.0"
      ],
      "line": 8
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "\u002720.0\u0027 are withdrawn from the account",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "the balance should be \u00270.0\u0027",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "the Transactions count should be equal to \u00274\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "the Withdraw RC should be 200",
  "keyword": "And "
});
formatter.match({
  "location": "WithdrawStepDefinitions.a_User_has_an_account_with_initial_transactions(String,String\u003e\u003e)"
});
formatter.result({
  "duration": 8673254,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "20.0",
      "offset": 1
    }
  ],
  "location": "WithdrawStepDefinitions.are_withdrawn_from_the_account(int)"
});
formatter.result({
  "duration": 11355074,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "0.0",
      "offset": 23
    }
  ],
  "location": "DepositStepDefinitions.the_balance_should_be(float)"
});
formatter.result({
  "duration": 3224940,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "4",
      "offset": 43
    }
  ],
  "location": "DepositStepDefinitions.the_Transactions_count_should_be_equal_to(long)"
});
formatter.result({
  "duration": 546970,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 26
    }
  ],
  "location": "WithdrawStepDefinitions.the_Withdraw_RC_should_be(int)"
});
formatter.result({
  "duration": 134283,
  "status": "passed"
});
formatter.before({
  "duration": 1048180,
  "status": "passed"
});
formatter.before({
  "duration": 1374908,
  "status": "passed"
});
formatter.before({
  "duration": 2648890,
  "status": "passed"
});
formatter.scenario({
  "line": 15,
  "name": "Withdrawing money with insufficient funds",
  "description": "",
  "id": "withdraw-money;withdrawing-money-with-insufficient-funds",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 16,
  "name": "a User has an account to debit with the initial transactions",
  "rows": [
    {
      "cells": [
        "date",
        "amount",
        "balance"
      ],
      "line": 17
    },
    {
      "cells": [
        "26/06/2016 10:10:20",
        "60.0",
        "60.0"
      ],
      "line": 18
    },
    {
      "cells": [
        "26/06/2016 12:30:20",
        "30.0",
        "90.0"
      ],
      "line": 19
    },
    {
      "cells": [
        "26/06/2016 19:49:11",
        "-70.0",
        "20.0"
      ],
      "line": 20
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 21,
  "name": "\u002721.0\u0027 are withdrawn from the account",
  "keyword": "When "
});
formatter.step({
  "line": 22,
  "name": "the Withdraw RC should be 403",
  "keyword": "Then "
});
formatter.match({
  "location": "WithdrawStepDefinitions.a_User_has_an_account_with_initial_transactions(String,String\u003e\u003e)"
});
formatter.result({
  "duration": 4668701,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "21.0",
      "offset": 1
    }
  ],
  "location": "WithdrawStepDefinitions.are_withdrawn_from_the_account(int)"
});
formatter.result({
  "duration": 3664569,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "403",
      "offset": 26
    }
  ],
  "location": "WithdrawStepDefinitions.the_Withdraw_RC_should_be(int)"
});
formatter.result({
  "duration": 138560,
  "status": "passed"
});
});