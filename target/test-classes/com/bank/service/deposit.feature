Feature: Save money
	
	Scenario: Depositing money on a new account
		Given a User has an account to credit with the initial transactions
			  | date                  | amount     | balance |
		When '50.0' are deposited into the account
		Then the balance should be '50.0'
			And the Transactions count should be equal to '1'
			And the Deposit RC should be 200
	
	Scenario: Depositing money on an existing account
		Given a User has an account to credit with the initial transactions
			  | date                  | amount       | balance   |
		      | 26/06/2016 10:10:20   | 60.0         | 60.0      |
		      | 26/06/2016 12:30:20   | 30.0         | 90.0      |
		      | 26/06/2016 19:49:11   | -70.0        | 20.0      |
		When '50.0' are deposited into the account
		Then the balance should be '70.0'
			And the Transactions count should be equal to '4'
			And the Deposit RC should be 200
	