package com.bank.serviceImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.bank.domain.Statement;
import com.bank.domain.StatementLine;
import com.bank.domain.Transaction;
import com.bank.repository.TransactionRepository;
import com.bank.service.TransactionService;

@Service
public class TransactionServiceImpl implements TransactionService {

	@Autowired private TransactionRepository txRepo;
	@Autowired private MongoOperations operations;
	
	/**
	 * @param accoutId
	 * @param year
	 * @param month
	 * @return Statement
	 * Finds the operation history from an account for a given month of the year
	 * 
	 */
	public Statement findByAccountIdAndYearAndMonth(String accountId, int year, int month) {
		if(accountId != null){
			Calendar cal = Calendar.getInstance();
			cal.set(year, month-1, 1, 0, 0, 0);
			Date minDate = cal.getTime();
			cal.add(Calendar.MONTH, 1);
			Date maxDate = cal.getTime();
			Statement statement = new Statement();
			List<StatementLine> lines = null;
			List<Transaction> r = txRepo.findByAccountIdAndBetweenDates(accountId, minDate, maxDate);
			if(r == null){
				return statement;
			}
			lines = new ArrayList<StatementLine>();
			for(Transaction tx : r){
				lines.add(new StatementLine(tx));
			}
			statement.setLines(lines);
			return statement;
		}
		return null;
	}

	/**
	 * @param accountId
	 * @return The total number of operations for a given account 
	 */
	public long countTransactionsByAccountId(String accountId) {
		if(accountId != null){
			return operations.count(new Query().addCriteria(Criteria.where("accountId").is(accountId)), Transaction.class);
		}
		return 0l;
	}

	/**
	 * @param accountId
	 * Deletes all the operations for a given account
	 */
	public void clearAccountTransaction(String accountId) {
		if(accountId !=null){
			operations.remove(new Query().addCriteria(Criteria.where("accountId").is(accountId)), Transaction.class);
		}
	}

	/**
	 * @param tx
	 * @return Transaction
	 * saves the Transaction and returns the saved object
	 */
	public Transaction save(Transaction tx) {
		if(tx == null || tx.getAccountId() == null || tx.getDate() == null){
			return null;
		}
		if(tx.getId() == null){
			tx.setId(UUID.randomUUID().toString());
		}
		return txRepo.save(tx);
	}

	/**
	 * @param accoutId
	 * @param minDate
	 * @param maxDate
	 * @return Statement
	 * Finds the operation history from an account for a given period
	 * minDate and maxDate are both included
	 */
	public Statement findByAccountIdBetweenDates(String accountId, Date minDate, Date maxDate) {

		if(accountId != null && minDate != null && maxDate != null){
			Statement statement = new Statement();
			List<StatementLine> lines = null;
			Calendar cal = Calendar.getInstance();
			cal.setTime(maxDate);
			cal.add(Calendar.DATE, 1);
			List<Transaction> r = txRepo.findByAccountIdAndBetweenDates(accountId, minDate, cal.getTime());
			if(r == null){
				return statement;
			}
			lines = new ArrayList<StatementLine>();
			for(Transaction tx : r){
				lines.add(new StatementLine(tx));
			}
			statement.setLines(lines);
			return statement;
		}
		return null;
	}

}
