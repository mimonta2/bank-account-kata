package com.bank.serviceImpl;

import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import com.bank.domain.Account;
import com.bank.domain.Response;
import com.bank.domain.Transaction;
import com.bank.repository.AccountRepository;
import com.bank.repository.TransactionRepository;
import com.bank.service.AccountService;

@Service
public class AccountServiceImpl implements AccountService {

	@Autowired private MongoOperations operations;
	@Autowired private AccountRepository repo;
	@Autowired private TransactionRepository txRepo;
	
	/**
	 * @param id
	 * @return Account
	 * Returns the account by its id
	 */
	public Account findById(String id) {
		
		return operations.findById(id, Account.class);
	}

	/**
	 * @param account
	 * @return Account
	 * Saves the accout object in the db and returns the saved object
	 */
	public Account save(Account account) {
		
		return repo.save(account);
	}

	/**
	 * @param accountId
	 * @param amount
	 * @return Response
	 * Performs a deposit operation and returns the operation response
	 * The Response contains the result code and message of the operation 
	 */
	public Response deposit(String accountId, float amount) {
		if(accountId != null && amount > 0f){
			Transaction tx = new Transaction(accountId, new Date(), amount);
			Calendar calendar = Calendar.getInstance();
			//20s is the timeout of each Transaction
			//Beyond this timeout, operations will be rolled back
			calendar.add(Calendar.SECOND, -20);
			Query query = new Query();
			query.addCriteria(Criteria.where("_id").is(accountId)
					.orOperator(Criteria.where("lockedBy").exists(false), Criteria.where("lockDate").exists(false), Criteria.where("lockDate").lt(calendar.getTime())));
			Update update = new Update();
			update.set("lockedBy", tx.getId());
			update.set("lockDate", new Date());
			//Lock the Balance
			Account account = operations.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), Account.class);
			if(account == null){
				//If the account is locked (used by another Transaction), Retry in 2s
				//Retry 10 times
				int retry = 0;
				while(account == null && retry < 10){
					try{
						Thread.sleep(2000);
					}catch(Exception e){
						return new Response(500, "Error has occured during the save");
					}
					account = operations.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), Account.class);
					retry++;
				}
				if(account == null){
					return new Response(501, "Error has occured during the save");
				}
			}

			//Prepare the query to unlock the balance
			query = new Query();
			query.addCriteria(Criteria.where("_id").is(accountId).and("lockedBy").is(tx.getId()));
			update = new Update();
			update.unset("lockedBy");
			update.unset("lockDate");
			
			//Add the Transaction balance
			tx.setBalance(account.getBalance() + amount);
			
			//If saving the Transaction fails
			if(txRepo.save(tx) == null){
				//Unlock the Balance
				query = new Query();
				query.addCriteria(Criteria.where("_id").is(accountId).and("lockedBy").is(tx.getId()));
				update = new Update();
				update.set("lockedBy", null);
				update.set("lockDate", null);
				operations.findAndModify(query, update, Account.class);
				return new Response(502, "Error has occured during the save");
			}
			update.inc("balance", amount);
			account = operations.findAndModify(query, update, Account.class);
			return new Response(200, "Deposit Success");
		}
		return new Response(400, "Bad Request!");
	}

	/**
	 * @param accountId
	 * @param amount
	 * @return Response
	 * Performs a withdraw operation and returns the operation response
	 * The Response contains the result code and message of the operation 
	 */
	public Response withdraw(String accountId, float amount) {
		if(accountId != null && amount > 0f){
			Transaction tx = new Transaction(accountId, new Date(), -amount);
			Calendar calendar = Calendar.getInstance();
			//20s is the timeout of each Transaction
			//Beyond this timeout, operations will be rolled back
			calendar.add(Calendar.SECOND, -20);
			Query query = new Query();
			query.addCriteria(Criteria.where("_id").is(accountId).and("lockedBy").exists(false)
					.orOperator(Criteria.where("lockDate").exists(false), Criteria.where("lockDate").lt(calendar.getTime())));
			Update update = new Update();
			update.set("lockedBy", tx.getId());
			update.set("lockDate", new Date());
			//Lock the Balance
			Account account = operations.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), Account.class);
			if(account == null){
				//If the account is locked (used by another Transaction), Retry in 2s
				//Retry 10 times
				int retry = 0;
				while(account == null && retry < 10){
					try{
						Thread.sleep(2000);
					}catch(Exception e){
						return new Response(500, "Error has occured during the save");
					}
					account = operations.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), Account.class);
					retry++;
				}
				if(account == null){
					return new Response(501, "Error has occured during the save");
				}
			}

			//Prepare the query to unlock the balance
			query = new Query();
			query.addCriteria(Criteria.where("_id").is(accountId).and("lockedBy").is(tx.getId()));
			update = new Update();
			update.unset("lockedBy");
			update.unset("lockDate");
			
			//Check if there are enough funds by comparing the balance to the amount
			if(account.getBalance() < amount){
				//Unlock the Balance
				operations.findAndModify(query, update, Account.class);
				return new Response(403, "Insufficient Funds");
			}
			
			//Insert the Transaction
			tx.setBalance(account.getBalance() - amount);
			
			//If saving the Transaction fails
			if(txRepo.save(tx) == null){
				//Unlock the Balance
				operations.findAndModify(query, update, Account.class);
				return new Response(502, "Error has occured during the save");
			}
			//Else, subtract the amount from the balance
			//And update it
			update.inc("balance", -amount);
			operations.findAndModify(query, update, Account.class);
			
			return new Response(200, "Deposit Success");
		}
		return new Response(400, "Bad Request!");
	}

}
