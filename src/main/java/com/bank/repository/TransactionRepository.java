package com.bank.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.bank.domain.Transaction;

@Repository
public interface TransactionRepository extends MongoRepository<Transaction, String>{

	@Query("{accountId : ?0, date : {$gte : ?1, $lt : ?2}}")
	public List<Transaction> findByAccountIdAndBetweenDates(String accountId, Date minDate, Date maxDate);

	
}
