package com.bank.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.bank.domain.Account;

@Repository
public interface AccountRepository extends MongoRepository<Account, String>{

}
