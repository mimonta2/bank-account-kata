package com.bank.domain;

import java.util.Date;
import java.util.UUID;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 
 * @author Montassar BAAZAOUI
 * This class corresponds to a Transaction processed on the client Account
 * the property <b>balance</b> corresponds to the balance of the client Account after this Transaction has been processed 
 */
@Document
public class Transaction {

	@Id private String id;
	private String accountId;
	private Date date;
	private float amount;
	private float balance;
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public float getAmount() {
		return amount;
	}
	public void setAmount(float amount) {
		this.amount = amount;
	}
	public float getBalance() {
		return balance;
	}
	public void setBalance(float balance) {
		this.balance = balance;
	}
	public Transaction(String accountId, Date date, float amount) {
		super();
		this.id = UUID.randomUUID().toString();
		this.accountId = accountId;
		this.date = date;
		this.amount = amount;
	}
	public Transaction() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	@Override
	public String toString() {
		return "Transaction [accountId=" + accountId + ", date=" + date
				+ ", amount=" + amount + ", balance=" + balance + "]";
	}
	public Transaction(String accountId, Date date, float amount, float balance) {
		super();
		this.accountId = accountId;
		this.date = date;
		this.amount = amount;
		this.balance = balance;
	} 
	
}
