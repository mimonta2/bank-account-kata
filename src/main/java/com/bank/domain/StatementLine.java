package com.bank.domain;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public class StatementLine {

	private Operation operation;
	private Date date;
	private float amount;
	private float balance;
	private final SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	
	public Operation getOperation() {
		return operation;
	}
	public void setOperation(Operation operation) {
		this.operation = operation;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public float getAmount() {
		return amount;
	}
	public void setAmount(float amount) {
		this.amount = amount;
	}
	public float getBalance() {
		return balance;
	}
	public void setBalance(float balance) {
		this.balance = balance;
	}
	public StatementLine(Transaction tx){
		this.operation = tx.getAmount() >= 0 ? Operation.C : Operation.D;
		this.amount = Math.abs(tx.getAmount());
		this.balance = tx.getBalance();
		this.date = tx.getDate();
	}
	public StatementLine(Map<String, String> tx) throws ParseException{
		this.operation = Operation.valueOf(tx.get("Operation"));
		this.amount = Float.parseFloat(tx.get("Amount"));
		this.balance = Float.parseFloat(tx.get("Balance"));
		this.date = formatter.parse(tx.get("Date"));
	}
	public StatementLine(){
		
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(amount);
		result = prime * result + Float.floatToIntBits(balance);
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result
				+ ((formatter == null) ? 0 : formatter.hashCode());
		result = prime * result
				+ ((operation == null) ? 0 : operation.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StatementLine other = (StatementLine) obj;
		if (Float.floatToIntBits(amount) != Float.floatToIntBits(other.amount))
			return false;
		if (Float.floatToIntBits(balance) != Float
				.floatToIntBits(other.balance))
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (formatter == null) {
			if (other.formatter != null)
				return false;
		} else if (!formatter.equals(other.formatter))
			return false;
		if (operation != other.operation)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "StatementLine [operation=" + operation + ", date=" + date
				+ ", amount=" + amount + ", balance=" + balance + "]";
	}
	
}
