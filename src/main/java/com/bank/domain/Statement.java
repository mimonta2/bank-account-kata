package com.bank.domain;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class Statement {

	private List<StatementLine> lines;

	public List<StatementLine> getLines() {
		return lines;
	}

	public void setLines(List<StatementLine> lines) {
		this.lines = lines;
	}
	
	public Statement(List<Map<String, String>> statement) throws ParseException{
		if(statement != null){
			lines = new ArrayList<StatementLine>();
			for(Map<String, String> statementLine : statement){
				lines.add(new StatementLine(statementLine));
			}
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		for( StatementLine sl : lines ){
		    result = result * prime + sl.hashCode();
		}
		return result;
	}

	public Statement() {
		super();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Statement other = (Statement) obj;
		if (lines == null) {
			if (other.lines != null)
				return false;
		} else if (!lines.equals(other.lines))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Statement [lines : \n" + Arrays.toString(lines.toArray()) + "]";
	}
	
}
