package com.bank.domain;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 
 * @author Montassar BAAZAOUI
 * This class corresponds to a Bank Account
 * The <b>balance</b> property contains the current balance of the account
 * 
 */
@Document
public class Account {

	@Id private String id;
	private String accessCode;
	private String firstname;
	private String lastname;
	//The balance of the account
	private float balance;
	//lockedBy is used to lock the Account while a Transaction is being processed
	private String lockedBy;
	//lockDate is used to check if the lock has expired or not
	private Date lockDate;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAccessCode() {
		return accessCode;
	}
	public void setAccessCode(String accessCode) {
		this.accessCode = accessCode;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public float getBalance() {
		return balance;
	}
	public void setBalance(float balance) {
		this.balance = balance;
	}
	public String getLockedBy() {
		return lockedBy;
	}
	public void setLockedBy(String lockedBy) {
		this.lockedBy = lockedBy;
	}
	@Override
	public String toString() {
		return "Account [id=" + id + ", balance=" + balance + ", lockedBy="
				+ lockedBy + "]";
	}
	public Date getLockDate() {
		return lockDate;
	}
	public void setLockDate(Date lockDate) {
		this.lockDate = lockDate;
	}
	
	
}
