package com.bank.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.bank.domain.Statement;
import com.bank.domain.Transaction;

@Service
public interface TransactionService {
	
	public Statement findByAccountIdAndYearAndMonth(String accountId, int year, int month);
	public long countTransactionsByAccountId(String accountId);
	public void clearAccountTransaction(String accountId);
	public Transaction save(Transaction tx);
	public Statement findByAccountIdBetweenDates(String id, Date dateMin, Date dateMax);
}
