package com.bank.service;

import org.springframework.stereotype.Service;

import com.bank.domain.Account;
import com.bank.domain.Response;

@Service
public interface AccountService {

	public Account findById(String id);
	public Account save(Account account);
	public Response deposit(String accountId, float amount);
	public Response withdraw(String accountId, float amount);
	
}
