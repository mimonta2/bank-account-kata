Feature: Print Transactions History
	
	Scenario: Printing an account statement
		Given a User has an account to print with the initial transactions
			  | date                  | amount      | balance  |
		      | 26/04/2016 10:10:20   | 60          | 60       |
		      | 27/04/2016 12:30:20   | 300         | 360      |
		      | 28/04/2016 19:49:11   | -60         | 300      |
		      | 03/05/2016 10:49:11   | -40         | 260      |
		      | 09/05/2016 12:39:11   | 220         | 480      |
		      | 15/05/2016 19:11:11   | -70         | 410      |
		      | 22/05/2016 13:22:11   | -90         | 320      |
		      | 15/06/2016 19:11:11   | 70          | 390      |
		      | 26/06/2016 13:22:11   | 90          | 480      |
		When the user prints the statement of the month '05/2016'
		Then the statement should be
			  | Operation | Date                  | Amount | Balance |
			  | D         | 03/05/2016 10:49:11   | 40     | 260     |
		      | C         | 09/05/2016 12:39:11   | 220    | 480     |
		      | D         | 15/05/2016 19:11:11   | 70     | 410     |
		      | D         | 22/05/2016 13:22:11   | 90     | 320     |
	
	
	Scenario: Printing an account statement
		Given a User has an account to print with the initial transactions
			  | date                  | amount      | balance  |
		      | 26/04/2016 10:10:20   | 60          | 60       |
		      | 27/04/2016 12:30:20   | 300         | 360      |
		      | 28/04/2016 19:49:11   | -60         | 300      |
		      | 03/05/2016 10:49:11   | -40         | 260      |
		      | 09/05/2016 12:39:11   | 220         | 480      |
		      | 15/05/2016 19:11:11   | -70         | 410      |
		      | 22/05/2016 13:22:11   | -90         | 320      |
		      | 15/06/2016 19:11:11   | 70          | 390      |
		      | 26/06/2016 13:22:11   | 90          | 480      |
		When the user prints the Operations History between '03/05/2016' and '15/05/2016'
		Then the statement should be
			  | Operation | Date                  | Amount | Balance |
			  | D         | 03/05/2016 10:49:11   | 40     | 260     |
		      | C         | 09/05/2016 12:39:11   | 220    | 480     |
		      | D         | 15/05/2016 19:11:11   | 70     | 410     |
	