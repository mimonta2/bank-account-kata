Feature: Withdraw money
	
	Scenario: Withdrawing money with sufficient funds
		Given a User has an account to debit with the initial transactions
			  | date                  | amount       | balance   |
		      | 26/06/2016 10:10:20   | 60.0         | 60.0      |
		      | 26/06/2016 12:30:20   | 30.0         | 90.0      |
		      | 26/06/2016 19:49:11   | -70.0        | 20.0      |
		When '20.0' are withdrawn from the account
		Then the balance should be '0.0'
			And the Transactions count should be equal to '4'
			And the Withdraw RC should be 200
	
	
	Scenario: Withdrawing money with insufficient funds
		Given a User has an account to debit with the initial transactions
			  | date                  | amount     | balance |
		      | 26/06/2016 10:10:20   | 60.0         | 60.0      |
		      | 26/06/2016 12:30:20   | 30.0         | 90.0      |
		      | 26/06/2016 19:49:11   | -70.0        | 20.0      |
		When '21.0' are withdrawn from the account
		Then the Withdraw RC should be 403
	