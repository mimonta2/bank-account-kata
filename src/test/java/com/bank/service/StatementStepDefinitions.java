package com.bank.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.bank.domain.Account;
import com.bank.domain.Response;
import com.bank.domain.Statement;
import com.bank.domain.Transaction;
import com.bank.test.config.AppConfig;

import cucumber.api.Format;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;

@ContextConfiguration(classes=AppConfig.class,loader=AnnotationConfigContextLoader.class)
public class StatementStepDefinitions {
 
	@Autowired private AccountService accountService;
	@Autowired private TransactionService txService;
	@Autowired private MongoOperations operations;
	private Account account;
	private Response r = new Response();
	private Statement statement;
	private final SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

	@Before
	public void beforeScenario() {
		operations.dropCollection("account");
		operations.dropCollection("transaction");
	}
	
	@Given("^a User has an account to print with the initial transactions$")
	public void a_User_has_an_account_with_initial_transactions(List<Map<String, String>> txs) throws Throwable {
    	account = new Account();
    	account.setId("USER_1");
    	account.setFirstname("Luka");
    	account.setFirstname("Modric");
    	accountService.save(account);
    	account = accountService.findById("USER_1");
    	assertNotNull("The account was not saved.", account);
    	
    	txService.clearAccountTransaction(account.getId());
		long initialTxsNumber = 0l;
		float balance = 0f;
		if(txs != null && !txs.isEmpty()){
			for(Map<String,String> tx : txs){
				Transaction txObject = new Transaction(account.getId(), formatter.parse(tx.get("date")), Float.parseFloat(tx.get("amount")), Float.parseFloat(tx.get("balance")));
				txService.save(txObject);
				balance = balance + txObject.getAmount();
			}
			account.setBalance(balance);
			accountService.save(account);
			initialTxsNumber = (txs == null ? 0 : txs.size());
			
		}
		long transactionsNumber = txService.countTransactionsByAccountId(account.getId());
        assertTrue("The Transaction was not inserted correctly. Expected "+initialTxsNumber+", found "+transactionsNumber, 
        		transactionsNumber == initialTxsNumber);

    	account = accountService.findById("USER_1");
    	assertTrue("The balance is not correct. Expected "+balance+", found "+account.getBalance(), 
        		balance == account.getBalance());
    }


	@When("^the user prints the statement of the month '(\\d+)/(\\d+)'$")
	public void the_user_prints_the_statement_of_the_month(int month, int year) throws Throwable {
		this.statement = txService.findByAccountIdAndYearAndMonth(account.getId(), year, month);
	    
	}
	
	@Then("^the statement should be$")
	public void the_statement_should_be(List<Map<String, String>> expectedResult) throws Throwable {
		try{
			Statement s = new Statement(expectedResult);
			assertTrue("Wrong returned operations. \nExpected :\n"+s+", \nFound :\n"+this.statement, s.equals(this.statement));
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@When("^the user prints the Operations History between '(.+)' and '(.+)'$")
	public void the_user_prints_the_Operations_History_between_and(@Format("dd/MM/yyyy") Date dateMin, @Format("dd/MM/yyyy") Date dateMax) throws Throwable {
	    this.statement = txService.findByAccountIdBetweenDates(account.getId(), dateMin, dateMax);
	}

}
