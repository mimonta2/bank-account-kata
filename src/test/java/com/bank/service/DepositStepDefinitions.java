package com.bank.service;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.bank.domain.Account;
import com.bank.domain.Response;
import com.bank.domain.Transaction;
import com.bank.repository.TransactionRepository;
import com.bank.test.config.AppConfig;

import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;

@ContextConfiguration(classes=AppConfig.class,loader=AnnotationConfigContextLoader.class)
public class DepositStepDefinitions {
 
	@Autowired private AccountService accountService;
	@Autowired private TransactionService txService;
	@Autowired private MongoOperations operations;
	private Account account;
	private Response r = new Response();
	private final SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	
	@Before
	public void beforeScenario() {
		operations.dropCollection("account");
		operations.dropCollection("transaction");
	}
	
	@Given("^a User has an account to credit with the initial transactions$")
	public void a_User_has_an_account_with_initial_transactions(List<Map<String, String>> txs) throws Throwable {
    	account = new Account();
    	account.setId("USER_1");
    	account.setFirstname("Luka");
    	account.setFirstname("Modric");
    	accountService.save(account);
    	account = accountService.findById("USER_1");
    	assertNotNull("The account was not saved.", account);
    	
    	txService.clearAccountTransaction(account.getId());
    	
		long initialTxsNumber = 0l;
		float balance = 0f;
		if(txs != null && !txs.isEmpty()){
			for(Map<String,String> tx : txs){
				Transaction txObject = new Transaction(account.getId(), formatter.parse(tx.get("date")), Float.parseFloat(tx.get("amount")), Float.parseFloat(tx.get("balance")));
				txService.save(txObject);
				balance = balance + txObject.getAmount();
			}
			account.setBalance(balance);
			accountService.save(account);
			initialTxsNumber = (txs == null ? 0 : txs.size());
			
		}
		long transactionsNumber = txService.countTransactionsByAccountId(account.getId());
        
		assertTrue("The Transaction was not inserted correctly. Expected "+initialTxsNumber+", found "+transactionsNumber, 
        		transactionsNumber == initialTxsNumber);

    	account = accountService.findById("USER_1");
    	assertTrue("The balance is not correct. Expected "+balance+", found "+account.getBalance(), 
        		balance == account.getBalance());
    }

    @When("^'(\\d+\\.\\d+)' are deposited into the account$")
    public void are_deposited_into_the_account(float amount) throws Throwable {
    	this.r = accountService.deposit("USER_1", amount);
    }

    @Then("^the balance should be '(\\d+\\.\\d+)'$")
    public void the_balance_should_be(float balance) throws Throwable {
    	account = accountService.findById("USER_1");
    	assertTrue("New Balance is wrong. Expected "+balance+", Found "+account.getBalance(), account.getBalance() == balance);
    }
    
	@Then("^the Transactions count should be equal to '(\\d+)'$")
	public void the_Transactions_count_should_be_equal_to(long count) throws Throwable {
		long transactionsNumber = txService.countTransactionsByAccountId(account.getId());
        assertTrue("The Transaction was not inserted correctly. Expected "+count+", found "+transactionsNumber, transactionsNumber == count);
        
	}
	
	@Then("^the Deposit RC should be (\\d+)$")
	public void the_Deposit_RC_should_be(int rc) throws Throwable {
	    assertTrue("The return code is not corret. Expected "+rc+", Found "+r.getRc(), rc == r.getRc());
	}

}
