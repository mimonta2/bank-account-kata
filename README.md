Overview
==========

This project is an answer to the Bank Account kata.
Frameworks used :
- Spring as my IoC container
- Spring Data for data persistence
- Mongodb as a db (I used fmongo to run the db in embedded mode. So no need to install a mongo instance)
- Cucumber for the BDD

User Stories
======

**US 1:**
--------
In order to save money<p>
As a bank client<p>
I want to make a deposit in my account<p>
<p>
<b>deposit.feature</b> file contains the test scenarios for this US
<p>
<p>
***US 2:***
--------

In order to retrieve some or all of my savings<p>
As a bank client<p>
I want to make a withdrawal from my account<p>
<p>
<b>withdraw.feature</b> file contains the test scenarios for this US
<p>
<p>
***US 3:***
--------

In order to check my operations<p>
As a bank client<p>
I want to see the history (operation, date, amount, balance)  of my operations<p>
<p>
<b>statement.feature</b> file contains the test scenarios for this US
<p>
<p>

To run the tests :

```
mvn test
```

